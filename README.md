
Please read the following docs:
- [Installing the application](docs/install.md)

Feel free to remove this file and provide your own documentation once your challenge is done.
# Application install

I used the skeleton project that you send to me. So to run it you can simply use docs
- [Installing the application](docs/install.md)

Or run it on any server with apache/nginx + php.

# Api documentation

#### All api's end-points lays in /api/v1/{url}

#### End-points

#### Method for all end-points is GET
````
/api/v1/vacancies/{id} - get vacancy by ID
````
#
````
/api/v1/vacancies/{location} - get vacancy by location
````
Location can be city or country ('berlin' or 'de')

To sort result simply add query param 'sort=ture'. 

````
/api/v1/vacancies/de?sort=true
````
#
````
/api/v1/vacancies/most_interesting - get most interesting vacancies by your skills, salary and tech stack.
````
You may pass 3 params in query

* skills[] - /api/v1/vacancies/most_interesting?skills[]=php&skills[]=symfony
* salary - /api/v1/vacancies/most_interesting?salary=500000
* level - /api/v1/vacancies/most_interesting?level=senior

#

### Most interesting position for me:
````
/api/v1/vacancies/most_interesting?skills[]=php&skills[]=symfony&skills[]=rest&salary=600000
````