<?php

namespace App\Tests\VacanciesDataSource;

use App\VacancyDataSource\CsvVacancyDataSource;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class CsvVacancyDataSourceTest extends TestCase
{
    private $cvsSource;

    public function setUp()
    {
        $this->cvsSource = $this->getMockBuilder(CsvVacancyDataSource::class)
          ->setMethods(array('__construct'))
          ->setConstructorArgs([])
          ->disableOriginalConstructor()
          ->getMock();

        $reflectionClass = new \ReflectionClass(CsvVacancyDataSource::class);
        $reflectionProperty = $reflectionClass->getProperty('data');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($this->cvsSource, $this->getArray());
    }

    public function tearDown()
    {
        $this->cvsSource = null;
    }

    public function testGetVacancyById()
    {
        $vacancy = $this->cvsSource->findVacancyById(1);
        $this->assertEquals(1, $vacancy['ID']);
    }

    public function testFindVacanciesByLocation()
    {
        $vacancies = $this->cvsSource->findVacanciesByLocation('de');
        $this->assertEquals(1, $vacancies[0]['ID']);

        $vacanciesByCity = $this->cvsSource->findVacanciesByLocation('berlin');
        $this->assertEquals(1, $vacanciesByCity[0]['ID']);
    }

    public function testFindAllVacancies()
    {
        $vacancies = $this->cvsSource->findAllVacancy();
        $this->assertEquals(2, count($vacancies));
    }

    public function testFindMostInterestingPosition()
    {
        $request = new Request();
        $skills = ['php', 'rest', 'symfony'];
        $request->query->add(['salary' => 600000, 'skills' => $skills]);
        $position = $this->cvsSource->findMostInterestingPosition($request);

        $this->assertEquals(1, $position[0]['ID']);
    }

    private function getArray()
    {
      return [
        0 => [
          'ID' => 1,
          'City' => 'Berlin',
            'Salary' => 650000,
            'Country' => 'DE',
            'Required skills' => '"PHP, Symfony, REST, Unit-testing, Behat, SOLID, Docker, AWS',
        ],
        1 => [
          'ID' => 2,
          'City' => 'Moscow',
          'Salary' => 300000,
            'Country' => 'RU'
        ]
      ];
    }
}