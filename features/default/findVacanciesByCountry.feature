Feature: Get vacancy by id end-point

  Scenario: Check that end-point returning vacancy by id is correct
    When I am on "/api/v1/vacancies/de"
    Then the response status code should be 200
    And COUNT should be equals 9