<?php

namespace App\Behat;

use Behat\MinkExtension\Context\RawMinkContext;
use PHPUnit\Framework\Assert;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawMinkContext
{
    /**
     * @Then ID should be equals :arg1
     */
    public function idShouldBeEquals($arg1)
    {
        $response = $this->getSession()->getPage()->getContent();

        $json = json_decode($response, true);

        Assert::assertEquals($arg1, $json['ID']);
    }

    /**
     * @Then COUNT should be equals 9
     */
    public function countShouldBeEquals()
    {
        $response = $this->getSession()->getPage()->getContent();

        $json = json_decode($response, true);

        Assert::assertEquals(9, count($json));
    }
}
