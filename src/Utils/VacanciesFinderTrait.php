<?php

namespace App\Utils;

/**
 * Trait VacanciesFinderTrait
 * Helpers functions to sort array.
 */
trait VacanciesFinderTrait
{
    /**
     * @param array  $rawData
     * @param string $param
     * @param array  $vacanciesArray
     *
     * @return array
     */
    public function vacanciesFindByParam(array $rawData, string $param, array $vacanciesArray): array
    {
        $sortedArray = [];

        foreach ($vacanciesArray as  $key => $value) {
            if (mb_strtolower($value) === $param) {
                $sortedArray[] = $rawData[$key];
            }
        }

        return $sortedArray;
    }

    /**
     * @param array $vacanciesArray
     *
     * @return array
     */
    public function vacancySort(array $vacanciesArray): array
    {
        return array_multisort(array_column($vacanciesArray, 'Seniority level'), SORT_ASC, array_column($vacanciesArray, 'Salary'), SORT_ASC, $vacanciesArray);
    }
}
