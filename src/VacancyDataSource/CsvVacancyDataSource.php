<?php

namespace App\VacancyDataSource;

use App\Utils\VacanciesFinderTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;

/**
 * Class CsvVacancyDataSource handle data from csv file.
 */
class CsvVacancyDataSource implements VacancyDataSourceInterface
{
    use VacanciesFinderTrait;

    /**
     * @var mixed
     */
    private $data;

    /**
     * CsvVacancyDataSource constructor.
     * Get csv file and decode it to array.
     *
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        $serializer = new Serializer([], [new CsvEncoder()]);
        $this->data = $serializer->decode(file_get_contents($filePath), 'csv');
    }

    /**
     * Find vacancy by it ID.
     *
     * @param int $id
     *
     * @return array
     */
    public function findVacancyById(int $id): array
    {
        $key = array_search($id, array_column($this->data, 'ID'));

        if (false !== $key) {
            return $this->data[$key];
        }

        return [];
    }

    /**
     * Find vacancies by country/city.
     *
     * @param string $location
     * @param bool   $sort
     *
     * @return array
     */
    public function findVacanciesByLocation(string $location, bool $sort = false): array
    {
        if (count($countryArray = $this->findVacancyByCountry($location, $sort))) {
            return $countryArray;
        }

        if (count($cityArray = $this->findVacancyByCity($location, $sort))) {
            return $cityArray;
        }

        return [];
    }

    /**
     * @return array
     */
    public function findAllVacancy(): array
    {
        return $this->data;
    }

    /**
     * Find most interesting position depends on your skills, level, salary
     * Its simply unset vacancies that does not match request query and
     * vacancy must have 2+ skills if you query with 'skills[]' param.
     *
     * @param Request $request
     *
     * @return array
     */
    public function findMostInterestingPosition(Request $request): array
    {
        if (empty($request->query->all())) {
            return $this->data;
        }

        $skills = [];
        $level = '';
        $salary = '';

        if ($request->query->has('skills')) {
            $vacancySkills = [];

            foreach ($request->query->get('skills') as $skill) {
                $skills[] = $skill;
            }

            foreach ($this->data as $key => $vacancy) {
                $vacancySkills[$key] = explode(', ', mb_strtolower($vacancy['Required skills']));
            }

            foreach ($vacancySkills as $key => $skillsArray) {
                if (count(array_intersect($skillsArray, $skills)) < 2) {
                    unset($this->data[$key]);
                }
            }
        }

        if ($request->query->has('level')) {
            $level = ucfirst($request->query->get('level'));
        }

        if ($request->query->has('salary')) {
            $salary = $request->query->get('salary');
        }

        foreach ($this->data as $key => $vacancy) {
            if ($vacancy['Salary'] < $salary && !empty($salary)) {
                unset($this->data[$key]);
            }

            if (!empty($level) && $vacancy['Seniority level'] != $level) {
                unset($this->data[$key]);
            }
        }

        return $this->data;
    }

    /**
     * Helper function to find vacancy by country and sort them.
     *
     * @param string $country
     * @param bool   $sort
     *
     * @return array
     */
    private function findVacancyByCountry(string $country, bool $sort = false): array
    {
        $vacancyByCountryColumns = array_column($this->data, 'Country');

        $vacancyByCountryArr = $this->vacanciesFindByParam($this->data, $country, $vacancyByCountryColumns);

        if (true === $sort) {
            array_multisort(array_column($vacancyByCountryArr, 'Seniority level'), SORT_ASC, array_column($vacancyByCountryArr, 'Salary'), SORT_ASC, $vacancyByCountryArr);
        }

        return $vacancyByCountryArr;
    }

    /**
     * Helper function to find vacancy by city and sort them.
     *
     * @param string $city
     * @param bool   $sort
     *
     * @return array
     */
    private function findVacancyByCity(string $city, bool $sort = false): array
    {
        $vacancyByCityColumns = array_column($this->data, 'City');

        $vacancyByCityArray = $this->vacanciesFindByParam($this->data, $city, $vacancyByCityColumns);

        if (true === $sort) {
            array_multisort(array_column($vacancyByCityArray, 'Seniority level'), SORT_ASC, array_column($vacancyByCityArray, 'Salary'), SORT_ASC, $vacancyByCityArray);
        }

        return $vacancyByCityArray;
    }
}
