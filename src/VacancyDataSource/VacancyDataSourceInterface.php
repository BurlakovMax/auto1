<?php

namespace App\VacancyDataSource;

use Symfony\Component\HttpFoundation\Request;

interface VacancyDataSourceInterface
{
    public function findVacancyById(int $id): array;

    public function findVacanciesByLocation(string $location, bool $sort): array;

    public function findAllVacancy(): array;

    public function findMostInterestingPosition(Request $request): array;
}
