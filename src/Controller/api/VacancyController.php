<?php

namespace App\Controller\api;

use App\DataSourceHandler;
use App\VacancyDataSource\CsvVacancyDataSource;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VacancyController
{
    private const FILE_PATH = '../data/vacancies.csv';
    /**
     * @var DataSourceHandler
     */
    private $dataSource;

    /**
     * VacancyController constructor.
     */
    public function __construct()
    {
        if (!file_exists(self::FILE_PATH)) {
            throw new FileException('No data source was found, try again later');
        }

        $this->dataSource = new DataSourceHandler(new CsvVacancyDataSource(self::FILE_PATH));
    }

    /**
     * @param int $id
     *
     * @return Response
     */
    public function findVacancyById(int $id): JsonResponse
    {
        $array = $this->dataSource->findVacancyById($id);

        if (!count($array)) {
            return new JsonResponse('No vacancy was found', Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($array, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function findMostInterestingVacancy(Request $request): JsonResponse
    {
        $result = $this->dataSource->findMostInterestingPosition($request);

        return new JsonResponse($result, Response::HTTP_OK);
    }

    /**
     * @param string  $location
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function findVacanciesByLocation(string $location, Request $request): JsonResponse
    {
        $sort = false;

        if ($request->query->has('sort')) {
            $sort = $request->query->get('sort');
        }

        return new JsonResponse($this->dataSource->findVacanciesByLocation($location, $sort), Response::HTTP_OK);
    }
}
