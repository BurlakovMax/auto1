<?php

namespace App;

use App\VacancyDataSource\VacancyDataSourceInterface;
use Symfony\Component\HttpFoundation\Request;

class DataSourceHandler
{
    private $dataSource;

    public function __construct(VacancyDataSourceInterface $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    public function findVacancyById(int $id): array
    {
        return $this->dataSource->findVacancyById($id);
    }

    public function findVacanciesByLocation(string $location, bool $sort): array
    {
        return $this->dataSource->findVacanciesByLocation($location, $sort);
    }

    public function findAllVacancy(): array
    {
        return $this->dataSource->findAllVacancy();
    }

    public function findMostInterestingPosition(Request $request): array
    {
        return $this->dataSource->findMostInterestingPosition($request);
    }
}
